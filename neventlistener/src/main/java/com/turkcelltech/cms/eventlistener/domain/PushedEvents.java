/**
 * 
 */
package com.turkcelltech.cms.eventlistener.domain;

import com.google.common.base.Objects;

import java.util.Arrays;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Dursun KOC
 * 
 */
@XmlRootElement(name = "events")
public class PushedEvents implements Iterable<PushedEvent> {

	private PushedEvent[] events;

	public PushedEvents() {

	}

	public PushedEvents(PushedEvent[] events) {
		this.events = events;
	}

	public int length() {
		return events != null ? events.length : 0;
	}

	@XmlElement(name = "event")
	public PushedEvent[] getEvents() {
		return events;
	}

	public void setEvents(PushedEvent[] events) {
		this.events = events;
	}

	@Override
	public String toString() {
        return Objects.toStringHelper(this).add("events",events).toString();
	}

	@Override
	public Iterator<PushedEvent> iterator() {
		return Arrays.asList(events).iterator();
	}
}
