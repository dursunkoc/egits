package com.turkcelltech.cms.eventlistener.persistence;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.turkcelltech.cms.eventlistener.config.Config;
import com.turkcelltech.cms.eventlistener.domain.EventHandler;

/**
 * Created with IntelliJ IDEA. User: TTDKOC Date: 10/12/13 Time: 4:23 PM To
 * change this template use File | Settings | File Templates.
 */
@Repository
public class EventHandlerRepositoryJdbcImpl implements EventHandlerRepository {
	private static final Logger logger = LoggerFactory
			.getLogger(FlowPoolRepositoryJdbcImpl.class);
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDatasource(DataSource datasource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(datasource);
	}

	@Override
	@Cacheable("eventHandler")
	public List<EventHandler> getActiveEventHandlersForEventTypeId(
			Long eventTypeId) {
		if (logger.isDebugEnabled()) {
			logger.debug("Starting query for eventId:'{}'", eventTypeId);
		}
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("eventTypeId", eventTypeId);
		paramSource.addValue("enabled", Config.CODE_ENABLED);
		paramSource.addValue("campaignStatusScheduled",
				Config.CODE_CAMPAIGN_STATUS_SCHEDULED);
		List<EventHandler> result = jdbcTemplate.query(
				EventHandler.QUERY_SELECT_ALL_BY_EVENT_TYPE_ID_AND_ACTIVE,
				paramSource, EventHandler.createRowMapper());
		if (logger.isDebugEnabled()) {
			logger.debug("{} records returned for eventTypeId:'{}'",
					result.size(), eventTypeId);
		}
		return result;
	}
}
