/**
 * 
 */
package com.turkcelltech.cms.eventlistener.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Dursun KOC
 * 
 */
@XmlRootElement
public class Events implements Iterable<Event>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7411790080890087833L;
	@XmlElement(name = "event")
	private Event[] events;

	public int length() {
		return events != null ? events.length : 0;
	}

	public Event[] getEvents() {
		return events;
	}

	@Override
	public String toString() {
		StringBuffer b = new StringBuffer("Event.Events (");
		b.append("events: [");
		if (events != null && events.length > 0) {
			for (Event event : events) {
				if (event != null) {
					b.append(event.toString());
					b.append(", ");
				}
			}
		}
		b.append("]");
		b.append(")");
		return b.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterator<Event> iterator() {
		return (Iterator<Event>) (events == null ? Collections.emptyList()
				: Arrays.asList(events)).iterator();
	}
}
