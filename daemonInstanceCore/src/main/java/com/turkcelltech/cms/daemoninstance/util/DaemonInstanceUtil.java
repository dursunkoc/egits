/**
 * 
 */
package com.turkcelltech.cms.daemoninstance.util;

import com.turkcelltech.cms.daemoninstance.config.Config;
import com.turkcelltech.cms.daemoninstance.config.MI_SCHEMA;
import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance;

/**
 * @author TTDKOC
 * 
 */
public class DaemonInstanceUtil {
	private DaemonInstanceUtil() {
	}

	public static String getSchemaName(DaemonInstance daemonInstance,
			MI_SCHEMA schema) {
		String schemaName = "";

		if (daemonInstance.getDaemonInstanceTypeId().equals(
				Config.DEFAULT_INSTANCE_TYPE_ID)) {
			schemaName = schema.defaultVal;
		} else {
			schemaName = daemonInstance.getSchemaName();
		}

		return schemaName;
	}
}
