/**
 * 
 */
package com.turkcelltech.cms.eventlistener.service;

import com.turkcelltech.cms.eventlistener.domain.Event;

/**
 * @author TTDKOC
 * 
 */
public interface EventService {

	public static enum EventSkipType {
		EVENT_CRITERIA, EVENT_HANDLER_CRITERIA;
	}

	/**
	 * @param event
	 * @param eventId
	 * @param eventChannel
	 * @param eventSkipType
	 */
	public void logSkippedEvent(Event event, Integer eventId,
			Integer eventChannel, EventSkipType eventSkipType);

	/**
	 * @param event
	 * @param eventId
	 * @param eventChannel
	 */
	public void logAsIncommingEvent(Event event, Integer eventId,
			Integer eventChannel);

	/**
	 * @param event
	 * @param eventId
	 * @param eventChannel
	 * @param e
	 */
	public void logAsException(Event event, Integer eventId,
			Integer eventChannel, RuntimeException e);

}
