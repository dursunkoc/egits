CREATE SCHEMA COMET AUTHORIZATION DBA
;
CREATE SCHEMA COMET_CORE AUTHORIZATION DBA
	CREATE TABLE DAEMONINSTANCE (
		ID                          NUMERIC            NOT NULL,
		NAME                        VARCHAR(100) NOT NULL,
		SCHEMANAME                  VARCHAR(30),
		DAEMONINSTANCETYPE_ID       NUMERIC            NOT NULL,
		CMSAPPLICATION_INSTANCE_ID  NUMERIC            NOT NULL
	)
	CREATE TABLE DAEMONINSTANCE_DECISIONVALUE(
	  ID                       NUMERIC               NOT NULL,
	  VALUE                    VARCHAR(400),
	  DAEMONINSTANCE_ID        NUMERIC,
	  DAEMONINSTANCETYPE_ID    NUMERIC               NOT NULL,
	  DAEMONINSTANCE_GROUP_ID  NUMERIC
	)
	CREATE TABLE DAEMONINSTANCE_GROUP(
	  ID                     NUMERIC,
	  NAME                   VARCHAR(100)     NOT NULL,
	  DAEMONINSTANCETYPE_ID  NUMERIC          NOT NULL
	)
	CREATE TABLE COMET_CORE.DAEMONINSTANCE_GROUP_MEMBERS(
	  ID                       NUMERIC,
	  DAEMONINSTANCE_GROUP_ID  NUMERIC               NOT NULL,
	  CPREFERED_INSTANCE       NUMERIC,
	  DAEMONINSTANCE_ID        NUMERIC               NOT NULL
	)
	CREATE TABLE TCODE (
		NCODE_ID     NUMERIC                           NOT NULL,
		TCODE        VARCHAR(50)                NOT NULL,
		CLANGCODE    NUMERIC                           NOT NULL,
		NVALUE       NUMERIC,
		TDESC        VARCHAR(200),
		CENABLED     NUMERIC                           NOT NULL,
		NORDER       NUMERIC,
		TUSER        VARCHAR(50)                NOT NULL,
		DDATE        DATE                             NOT NULL,
		TMODIFYUSER  VARCHAR(50)                NOT NULL,
		DMODIFYDATE  DATE                             NOT NULL
	)
	CREATE TABLE COMET_CORE.TCAMPAIGNDEFINITION(
		NCAMPAIGNDEFINITION_ID  NUMERIC                NOT NULL,
		NCAMPAIGN_ID            NUMERIC                NOT NULL,
		NCAMPAIGNGROUP_ID       NUMERIC,
		NCAMPAIGNGROUPORDER     NUMERIC,
		NCONTEXT_ID             NUMERIC,
		CCAMPAIGNTYPE           NUMERIC                NOT NULL,
		CCAMPAIGNSTATUS         NUMERIC                NOT NULL,
		DSTARTDATE              DATE                  NOT NULL,
		DENDDATE                DATE                  NOT NULL,
		NLASTCOUNT              NUMERIC,
		DLASTCOUNTDATE          DATE,
		TUSER                   VARCHAR(50)     NOT NULL,
		DDATE                   DATE                  NOT NULL,
		TMODIFYUSER             VARCHAR(50)     NOT NULL,
		DMODIFYDATE             DATE                  NOT NULL,
		NPROCESS_PRIORITY       NUMERIC                DEFAULT 2                     NOT NULL,
		CCAMPAIGNRUNTYPE        NUMERIC                DEFAULT 0                     NOT NULL,
		CCAMPAIGNDELAYEDFLAG    NUMERIC                DEFAULT 0                     NOT NULL
	)
;