/**
 * 
 */
package com.turkcelltech.cms.daemoninstance.persistance;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.turkcelltech.cms.daemoninstance.config.Config;
import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance;

/**
 * @author TTDKOC
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testConfiguration.xml")
public class DaemonInstanceRepositoryJdbcImplTest {
	@Autowired
	private DaemonInstanceRepository daemonInstanceRepository;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.turkcelltech.cms.daemoninstance.persistance.DaemonInstanceRepositoryJdbcImpl#getDaemonInstanceByName(java.lang.String)}.
	 */
	@Test
	public void testGetDaemonInstanceByName() {
		DaemonInstance daemonInstanceByName = daemonInstanceRepository
				.getDaemonInstanceByName("OUTBOUND_1");
		Assert.assertNotNull("Daemon instance definition should exists!",
				daemonInstanceByName);
	}

	/**
	 * Test method for {@link com.turkcelltech.cms.daemoninstance.persistance.DaemonInstanceRepositoryJdbcImpl#getDaemonCurrentInstance()}.
	 */
	@Test
	public void testGetDaemonCurrentInstance() {
		DaemonInstance daemonCurrentInstance = daemonInstanceRepository
				.getDaemonCurrentInstance();
		Assert.assertNotNull(
				"Default Daemon instance definition should exists!",
				daemonCurrentInstance);
		Assert.assertEquals("DEFAULT", daemonCurrentInstance.getName());
	}

	/**
	 * Test method for {@link com.turkcelltech.cms.daemoninstance.persistance.DaemonInstanceRepositoryJdbcImpl#getDaemonInstanceFor(java.lang.Long, java.lang.String)}.
	 */
	@Test
	public void testGetDaemonInstanceFor() {
		DaemonInstance daemonInstanceFor = daemonInstanceRepository.getDaemonInstanceFor(
Config.OUTBOUND_INSTANCE_TYPE_ID, "1");
		Assert.assertNotNull(
				"DaemonInstance for a specific outbound campaign type should exist.",
				daemonInstanceFor);
		Assert.assertEquals("Daemon Instance should be outbound campaign type",
				Config.OUTBOUND_INSTANCE_TYPE_ID,
				daemonInstanceFor.getDaemonInstanceTypeId());
	}

}
