/**
 *
 */
package com.turkcelltech.cms.eventlistener.rs;

import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aric.myel.statements.components.Environment;
import com.google.common.collect.ObjectArrays;
import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance;
import com.turkcelltech.cms.daemoninstance.persistance.DaemonInstanceRepository;
import com.turkcelltech.cms.eventlistener.domain.Event;
import com.turkcelltech.cms.eventlistener.domain.EventAttribute;
import com.turkcelltech.cms.eventlistener.domain.EventAttributeWithValue;
import com.turkcelltech.cms.eventlistener.domain.EventHandler;
import com.turkcelltech.cms.eventlistener.domain.EventType;
import com.turkcelltech.cms.eventlistener.domain.Events;
import com.turkcelltech.cms.eventlistener.domain.FlowPool;
import com.turkcelltech.cms.eventlistener.domain.HandledSingleEvent;
import com.turkcelltech.cms.eventlistener.domain.PushedEvent;
import com.turkcelltech.cms.eventlistener.domain.PushedEvent.STATUS;
import com.turkcelltech.cms.eventlistener.domain.PushedEvents;
import com.turkcelltech.cms.eventlistener.persistence.EventHandlerRepository;
import com.turkcelltech.cms.eventlistener.persistence.EventTypeRepository;
import com.turkcelltech.cms.eventlistener.persistence.FlowPoolRepository;
import com.turkcelltech.cms.eventlistener.service.EventHandlerService;
import com.turkcelltech.cms.eventlistener.service.EventService;
import com.turkcelltech.cms.eventlistener.service.EventService.EventSkipType;
import com.turkcelltech.cms.eventlistener.util.EventUtil;
import com.turkcelltech.cms.eventlistener.util.EventUtil.ValidationResult;
import com.turkcelltech.cms.utils.ArrayUtil;

/**
 * @author Dursun KOC
 */
@Controller
@RequestMapping("/events")
public final class EventController {
	private Logger logger = LoggerFactory.getLogger(EventController.class);
	@Autowired
	private EventTypeRepository eventTypeRepository;

	@Autowired
	private EventHandlerRepository eventHandlerRepository;
	@Autowired
	private EventService eventService;
	@Autowired
	private FlowPoolRepository flowPoolRepository;
	@Qualifier("eventHandlerServiceThreadPoolImpl")
	@Autowired
	private EventHandlerService eventHandlerService;

	@Autowired
	private DaemonInstanceRepository daemonInstanceRepository;

	/**
	 * @param eventId
	 * @param eventChannel
	 * @param events
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/{eventId}/{eventChannel}", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public final ResponseEntity<PushedEvents> pushEvent(
			@PathVariable Long eventId, @PathVariable String eventChannel,
			@RequestBody Events events) {
		logger.debug("{} events received----------------------|",
				events.length());
		final PushedEvent[] pushedEvents = new PushedEvent[events.length()];

		// TODO_LOW get eventType and parameters at one invocation
		EventType eventType = eventTypeRepository.getEventType(eventId,
				eventChannel);
		logger.debug("EventType:> {}", eventType);

		List<EventAttribute> eventAttributes = eventTypeRepository
				.getEventTypeParameters(eventType);

		DaemonInstance daemonInstanceForEventType = daemonInstanceRepository
				.getDaemonInstanceFor(
						com.turkcelltech.cms.daemoninstance.config.Config.EVENT_INSTANCE_TYPE_ID,
						eventType.getId().toString());
		int i = 0;
		FlowPool[] finalFlowPools = new FlowPool[0];

		for (Event event : events) {
			logger.debug("{}. event is being processed.", i);
			final HandledSingleEvent handledSingleEvent = handleSingleEvent(
					event, eventType, eventAttributes);
			pushedEvents[i++] = handledSingleEvent.getPushedEvent();
			FlowPool[] flowPools = handledSingleEvent.getFlowPools();
			if (flowPools != null) {
				finalFlowPools = ObjectArrays.concat(finalFlowPools, flowPools,
						FlowPool.class);
			}
			logger.debug("{}. event was processed.", (i - 1));
		}
		logger.debug(" events processed---------------------|", events.length());

		flowPoolRepository.insertFlowPools(daemonInstanceForEventType,
				finalFlowPools);

		return new ResponseEntity<PushedEvents>(new PushedEvents(pushedEvents),
				HttpStatus.OK);
	}

	/**
	 * @param date
	 * @param event
	 * @param eventAttributes
	 * @param eventId
	 * @param eventChannel
	 * @return see assumption#2 String failedEventKey = getFailedEventKey(event,
	 *         eventId); Set<String> failedContextKeys, if
	 *         (failedContextKeys.contains(failedEventKey)) { return new
	 *         PushedEvent(event.getEventSourceId(), PushedEvent.STATUS.FAIL,
	 *         "PREVIOUS RECORD FAILED!"); }
	 */
	private HandledSingleEvent handleSingleEvent(Event event,
			EventType eventType, List<EventAttribute> eventAttributes) {
		logger.debug("Processing event:> {}", event);
		Integer eventId = eventType.getEventId();
		Integer eventChannel = eventType.getEventAgent();
		eventService.logAsIncommingEvent(event, eventId, eventChannel);
		try {
			ValidationResult validatedParameters = EventUtil
					.validateParameters(eventType, eventAttributes,
							event.getParameters());

			boolean valid = validatedParameters.isValid();
			logger.debug("Valid:> {}", valid);
			if (!valid) {
				return new HandledSingleEvent(new PushedEvent(
						event.getEventSourceId(), STATUS.FAIL,
						validatedParameters.result), null);
			}
			List<EventAttributeWithValue> attributeWithValues = validatedParameters.attributeWithValues;
			FlowPool[] flowPools = null;
			Environment env = EventUtil.generateEnvironment(event,
					attributeWithValues);
			if (EventUtil.eventCriteriaSatisfied(eventType, env)) {
				flowPools = handleSingleEventInternal(eventType, event,
						attributeWithValues, env);
				if (ArrayUtil.nullOrEmptyOrAllNull(flowPools)) {
					eventService.logSkippedEvent(event, eventId, eventChannel,
							EventSkipType.EVENT_HANDLER_CRITERIA);
				}
			} else {
				eventService.logSkippedEvent(event, eventId, eventChannel,
						EventSkipType.EVENT_CRITERIA);
			}
			return new HandledSingleEvent(new PushedEvent(
					event.getEventSourceId(), STATUS.OK, ""), flowPools);
		} catch (RuntimeException e) {
			eventService.logAsException(event, eventId, eventChannel, e);
			throw e;
		}

	}

	/**
	 * @param eventType
	 * @param event
	 * @param eventId
	 * @param eventChannel
	 * @param attributeWithValues
	 * @param env
	 */
	private FlowPool[] handleSingleEventInternal(EventType eventType,
			Event event, List<EventAttributeWithValue> attributeWithValues,
			Environment env) {
		List<EventHandler> eventHandlers = eventHandlerRepository
				.getActiveEventHandlersForEventTypeId(eventType.getId());
		int eventHandlersSize = eventHandlers.size();
		@SuppressWarnings("unchecked")
		Future<FlowPool>[] handleResults = new Future[eventHandlersSize];
		int index = 0;
		for (EventHandler eventHandler : eventHandlers) {
			handleResults[index++] = eventHandlerService.handle(eventHandler,
					event, attributeWithValues, env);
		}
		FlowPool[] result = new FlowPool[eventHandlersSize];
		int i = 0;
		for (Future<FlowPool> handleResult : handleResults) {
			try {
				final FlowPool flowPool = handleResult.get(1, TimeUnit.MINUTES);
				result[i++] = flowPool;
			} catch (Exception e) {
				logger.error("Error Occured while handling event: " + event, e);
				throw new RuntimeException(e);
			}
		}
		return result;
	}

}
