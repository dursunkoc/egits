package com.turkcelltech.cms.eventlistener.file;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventProcessor implements Processor {
	private static final Logger LOG = LoggerFactory.getLogger(EventProcessor.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		LOG.warn("INPUT >>>> "+exchange.toString());
		// TODO_LOW vey low Process file
	}

}
