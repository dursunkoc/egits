/**
 * 
 */
package com.turkcelltech.cms.daemoninstance.persistance;

import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance;

/**
 * @author TTDKOC
 *
 */
public interface DaemonInstanceRepository {

	/**
	 * @param name
	 * @return
	 */
	public DaemonInstance getDaemonInstanceByName(String name);

	/**
	 * @return
	 */
	public DaemonInstance getDaemonCurrentInstance();

	/**
	 * @param instanceTypeId
	 * @param decisionValue
	 * @return
	 */
	public DaemonInstance getDaemonInstanceFor(Long instanceTypeId,
			String decisionValue);
}
