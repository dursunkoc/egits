CMS Event Listener
==========================================================

You can build the WAR by running

    mvn install

You can then run the project by dropping the WAR into your 
favorite web container or just run

    mvn jetty:run

to start up and deploy to Jetty.

You need to create OS env variable $CMS_HOME denoting the 
CMS home directory

For configuring camel you need to create a camel.properties 
file in the "${CMS_HOME}/config/camel/" path; a sample 
content for it as follows;
##########################################################
eventpath=D:\\DEV\\homes\\cmshome\\
##########################################################
    
You need to create a logback-included.xml file in the 
"${CMS_HOME}/config/" path; a sample content for it as 
follows;
-----------------------------------------------------------------------------------------
<included>
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} - %msg%n</pattern>
        </encoder>
    </appender>

	<appender name="eventExceptionLogger"
		class="ch.qos.logback.core.rolling.RollingFileAppender">
		<file>${CMS_HOME}\log\eventExceptionLogger.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- daily rollover -->
			<fileNamePattern>${CMS_HOME}\log\eventExceptionLogger.%d{yyyy-MM-dd}.log
			</fileNamePattern>
			<!-- keep 30 days' worth of history -->
			<maxHistory>30</maxHistory>
		</rollingPolicy>
		<encoder>
			<pattern>%d{HH:mm:ss.SSS} - %msg%n</pattern>
		</encoder>
	</appender>
    <logger name="eventExceptionLogger">
        <level value="INFO"/>
        <appender-ref ref="eventExceptionLogger"/>
    </logger>
	<appender name="eventInLogger"
		class="ch.qos.logback.core.rolling.RollingFileAppender">
		<file>${CMS_HOME}\log\eventInLogger.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- daily rollover -->
			<fileNamePattern>${CMS_HOME}\log\eventInLogger.%d{yyyy-MM-dd}.log
			</fileNamePattern>
			<!-- keep 30 days' worth of history -->
			<maxHistory>30</maxHistory>
		</rollingPolicy>
		<encoder>
			<pattern>%d{HH:mm:ss.SSS} - %msg%n</pattern>
		</encoder>
	</appender>
    <logger name="eventInLogger">
        <level value="INFO"/>
        <appender-ref ref="eventInLogger"/>
    </logger>

	<appender name="eventSkipLogger"
		class="ch.qos.logback.core.rolling.RollingFileAppender">
		<file>${CMS_HOME}\log\eventSkipLogger.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- daily rollover -->
			<fileNamePattern>${CMS_HOME}\log\eventSkipLogger.%d{yyyy-MM-dd}.log
			</fileNamePattern>
			<!-- keep 30 days' worth of history -->
			<maxHistory>30</maxHistory>
		</rollingPolicy>
		<encoder>
			<pattern>%d{HH:mm:ss.SSS} - %msg%n</pattern>
		</encoder>
	</appender>
    <logger name="eventSkipLogger">
        <level value="INFO"/>
        <appender-ref ref="eventSkipLogger"/>
    </logger>

    <logger name="org.springframework">
        <level value="WARN"/>
        <appender-ref ref="STDOUT"/>
    </logger>
    <logger name="org.springframework.web.context.ContextLoader">
        <level value="WARN"/>
        <appender-ref ref="STDOUT"/>
    </logger>

    <logger name="com.turkcelltech.eventlistener.service">
        <level value="ERROR"/>
        <appender-ref ref="STDOUT"/>
    </logger>

    <logger name="com.turkcelltech.eventlistener.rs.EventController">
        <level value="ERROR"/>
        <appender-ref ref="STDOUT"/>
    </logger>
</included>
-----------------------------------------------------------------------------------------
==========================================================
For testing Rest Services you can use following curl requests;
==========================================================
XML -> XML
curl -w "\nTime: %{time_total}" -X POST -H "Content-Type:application/xml; Accept:application/xml" -d "<events><event><eventSourceId>1</eventSourceId><contextKey>535</contextKey><date>1378502969000</date><parameters><entry><key>name</key><value>Dursun</value></entry><entry><key>age</key><value>30</value></entry></parameters></event></events>" http://localhost:8080/rs/events/1/BSCS
<events><event><eventSourceId>1</eventSourceId><contextKey>535</contextKey><date>1378502969000</date><parameters><entry><key>name</key><value>Dursun</value></entry><entry><key>age</key><value>30</value></entry></parameters></event></events>

JSON -> JSON
curl -w "\nTime: %{time_total}" -X POST -H "Content-Type:application/json; Accept:application/json" -d "{\"events\":[{\"eventSourceId\":1,\"contextKey\":\"535\",\"date\":1378502969000,\"parameters\":{\"name\":\"Dursun\",\"age\":30}}]}" http://localhost:8080/rs/events/1/BSCS
{"events":[{"eventSourceId":1,"contextKey":"535","date":1378502969000,"parameters":{"name":"Dursun","age":30}}]}


==========================================================
You need to install Oracle jdbc driver dependencies by 
yourself as followed
==========================================================
download ==> ojdbc6.jar and orai18n.jar from "http://www.oracle.com/technetwork/database/enterprise-edition/jdbc-112010-090769.html"
version "Oracle Database 11g Release 2 (11.2.0.4) JDBC Drivers"
and run following mvn commands
mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.4 -Dpackaging=jar -Dfile=ojdbc6.jar -DgeneratePom=true
mvn install:install-file -DgroupId=com.oracle -DartifactId=orai18n -Dversion=11.2.0.4 -Dpackaging=jar -Dfile=orai18n.jar -DgeneratePom=true