/**
 * 
 */
package com.turkcelltech.cms.eventlistener.persistence;

import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.turkcelltech.cms.eventlistener.domain.EventAttribute;
import com.turkcelltech.cms.eventlistener.domain.EventType;
import com.turkcelltech.cms.utils.QueryUtil;

/**
 * @author TTDKOC
 * 
 */
@Service
public class EventTypeRepositoryJdbcImpl implements EventTypeRepository {
	private static final Logger logger = LoggerFactory
			.getLogger(FlowPoolRepositoryJdbcImpl.class);
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDatasource(DataSource datasource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(datasource);
	}

	@Override
	@CacheEvict("eventType")
	public void flushEventTypeCache() {
		logger.info("Flushing EventType Cache");
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.turkcelltech.eventlistener.persistence.EventTypeRepository#getEventType(
	 * java.lang.Long, java.lang.String)
	 */
	@Override
	@Cacheable("eventType")
	public EventType getEventType(Long eventId, String eventChannel) {
		if (logger.isDebugEnabled()) {
			logger.debug("Starting query for eventId:'{}' eventChannel:'{}'",
					eventId, eventChannel);
		}

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("eventId", eventId);
		paramSource.addValue("eventAgent", eventChannel);
		List<EventType> queryResult = jdbcTemplate.query(
				EventType.QUERY_SELECT_ALL_BY_EVENT_ID_AND_EVENT_CHANNEL,
				paramSource,
				EventType.createRowMapper());
		EventType result = QueryUtil.oneAndOnlyOne(queryResult, "EventType",
				eventId, eventChannel);

		if (logger.isDebugEnabled()) {
			logger.debug("{} is returned for eventId:'{}' eventChannel:'{}'",
					new Object[] { result, eventId, eventChannel });
		}
		return result;
	}

	@Override
	@CacheEvict("eventTypeParameters")
	public void flushEventTypeParametersCache() {
		logger.info("Flushing EventTypeParameters Cache");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.turkcelltech.eventlistener.persistence.EventTypeRepository#
	 * getEventTypeParameters(com.turkcelltech.eventlistener.domain.EventType)
	 */
	@Override
	@Cacheable("eventTypeParameters")
	public List<EventAttribute> getEventTypeParameters(EventType eventType) {
		if (logger.isDebugEnabled())
			logger.debug("Starting query for eventType:'" + eventType + "'");
		if (eventType == null) {
			return Collections.<EventAttribute> emptyList();
		}
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("eventTypeId", eventType.getId());

		List<EventAttribute> result = jdbcTemplate.query(
				EventAttribute.QUERY_SELECT_ALL_BY_EVENT_TYPE_ID,
				paramSource, EventAttribute.createRowMapper());

		if (logger.isDebugEnabled()) {
			logger.debug(result.size() + " records returned for eventType:'"
					+ eventType + "'");
		}
		return result;
	}

}
