/**
 * 
 */
package com.turkcelltech.cms.eventlistener.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Dursun KOC
 * 
 */
@XmlType
public final class PushedEvent {
	@XmlEnum
	public static enum STATUS {
		OK, FAIL
	}

	@XmlElement
	private final Long id;
	@XmlElement
	private final STATUS status;
	@XmlElement
	private final String description;

	/**
	 * @param id
	 * @param status
	 */
	public PushedEvent(Long id, STATUS status, String description) {
		this.id = id;
		this.status = status;
		this.description = description;
	}

	/**
     * 
     */
	public PushedEvent() {
		this(null, null, null);
	}

	public Long getId() {
		return id;
	}

	public STATUS getStatus() {
		return status;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuffer b = new StringBuffer("PushedEvent (");
		b.append("id:").append(this.id);
		b.append(", status:").append(this.status);
		b.append(", description:").append(this.description);
		b.append(")");
		return b.toString();
	}

}
