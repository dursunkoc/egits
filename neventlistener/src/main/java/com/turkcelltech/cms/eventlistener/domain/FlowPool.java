package com.turkcelltech.cms.eventlistener.domain;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Objects;
import com.turkcelltech.cms.daemoninstance.config.Config;

/**
 * Created with IntelliJ IDEA. User: TTDKOC Date: 10/11/13 Time: 4:32 PM To
 * change this template use File | Settings | File Templates.
 */
public class FlowPool implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8281142267822696943L;
	// TODO_HIGH initenvironment to FlowPool for other processors
	// ALTER TABLE COMET_FE.TFLOWPOOL ADD (TENVIRONMENTVALUE VARCHAR2(4000))
	// ALTER TABLE COMET_FE.TFLOWPOOL DROP COLUMN TENVIRONMENTVALUE
	public static String INSERT_SQL = "INSERT INTO "
			+ Config.SCHEMA_HOLDER
			+ ".TFLOWPOOL (DDATE,NFLOWPOOL_ID,NNEXTNODE_ID,NCURRENTTRYCOUNT,NSOURCE_ID,NFLOWID,TCONTEXTKEY,NPROCESSEXCEPTIONID,CFLOWPOOLSOURCETYPE,CFLOWPOOLSTATUS,DNEXTEXECUTIONDATE,DMODIFYDATE,TENVIRONMENTVALUE)  VALUES (:date,COMET_FE.SEQ_NFLOWPOOL_ID.nextval,:nextNodeId,:currentTryCount,:eventSourceId,:flowId,:contextKey,:processExceptionId,:flowPoolSourceType,:flowPoolStatus,:nextExecutionDate,:modifyDate,:flowInitEnvironment )";
	private static final Long FIX_CURRENTTRYCOUNT = 0l;
	private static final Integer FIX_FLOWPOOLSOURCETYPE = 3;
	private static final Integer FIX_FLOWPOOLSTATUS = 1;
	private Long id;// flowPoolId
	private Long nextNodeId;// nextNodeId
	private Long currentTryCount;// currentTryCount
	private Long eventSourceId;// sourceId
	private Long flowId;// flowId
	private String contextKey;// contextKey
	private Long processExceptionId;// processExceptionId
	private Integer flowPoolSourceType;// flowPoolSourceType
	private Integer flowPoolStatus;// flowPoolStatus
	private Date date;// date
	private Date nextExecutionDate;// nextExecutionDate
	private Date modifyDate;// modifyDate
	private String flowInitEnvironment;//

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nextNodeId
	 */
	public Long getNextNodeId() {
		return nextNodeId;
	}

	/**
	 * @param nextNodeId
	 *            the nextNodeId to set
	 */
	public void setNextNodeId(Long nextNodeId) {
		this.nextNodeId = nextNodeId;
	}

	/**
	 * @return the currentTryCount
	 */
	public Long getCurrentTryCount() {
		return currentTryCount;
	}

	/**
	 * @param currentTryCount
	 *            the currentTryCount to set
	 */
	public void setCurrentTryCount(Long currentTryCount) {
		this.currentTryCount = currentTryCount;
	}

	/**
	 * @return the eventSourceId
	 */
	public Long getEventSourceId() {
		return eventSourceId;
	}

	/**
	 * @param eventSourceId
	 *            the eventSourceId to set
	 */
	public void setEventSourceId(Long eventSourceId) {
		this.eventSourceId = eventSourceId;
	}

	/**
	 * @return the flowId
	 */
	public Long getFlowId() {
		return flowId;
	}

	/**
	 * @param flowId
	 *            the flowId to set
	 */
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	/**
	 * @return the contextKey
	 */
	public String getContextKey() {
		return contextKey;
	}

	/**
	 * @param contextKey
	 *            the contextKey to set
	 */
	public void setContextKey(String contextKey) {
		this.contextKey = contextKey;
	}

	/**
	 * @return the processExceptionId
	 */
	public Long getProcessExceptionId() {
		return processExceptionId;
	}

	/**
	 * @param processExceptionId
	 *            the processExceptionId to set
	 */
	public void setProcessExceptionId(Long processExceptionId) {
		this.processExceptionId = processExceptionId;
	}

	/**
	 * @return the flowPoolSourceType
	 */
	public Integer getFlowPoolSourceType() {
		return flowPoolSourceType;
	}

	/**
	 * @param flowPoolSourceType
	 *            the flowPoolSourceType to set
	 */
	public void setFlowPoolSourceType(Integer flowPoolSourceType) {
		this.flowPoolSourceType = flowPoolSourceType;
	}

	/**
	 * @return the flowPoolStatus
	 */
	public Integer getFlowPoolStatus() {
		return flowPoolStatus;
	}

	/**
	 * @param flowPoolStatus
	 *            the flowPoolStatus to set
	 */
	public void setFlowPoolStatus(Integer flowPoolStatus) {
		this.flowPoolStatus = flowPoolStatus;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the nextExecutionDate
	 */
	public Date getNextExecutionDate() {
		return nextExecutionDate;
	}

	/**
	 * @param nextExecutionDate
	 *            the nextExecutionDate to set
	 */
	public void setNextExecutionDate(Date nextExecutionDate) {
		this.nextExecutionDate = nextExecutionDate;
	}

	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * @param modifyDate
	 *            the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	/**
	 * @return the flowInitEnvironment
	 */
	public String getFlowInitEnvironment() {
		return flowInitEnvironment;
	}

	/**
	 * @param flowInitEnvironment
	 *            the flowInitEnvironment to set
	 */
	public void setFlowInitEnvironment(String flowInitEnvironment) {
		this.flowInitEnvironment = flowInitEnvironment;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id)
				.add("nextNodeId", nextNodeId)
				.add("currentTryCount", currentTryCount)
				.add("eventSourceId", eventSourceId).add("flowId", flowId)
				.add("contextKey", contextKey)
				.add("processExceptionId", processExceptionId)
				.add("flowPoolSourceType", flowPoolSourceType)
				.add("flowPoolStatus", flowPoolStatus).add("date", date)
				.add("nextExecutionDate", nextExecutionDate)
				.add("modifyDate", modifyDate)
				.add("flowInitEnvironment", flowInitEnvironment).toString();
	}

	private FlowPool() {
	}

	/**
	 * @return
	 */
	private static FlowPool createWithDefaults() {
		final Date now = new Date();
		final FlowPool flowPool = new FlowPool();
		flowPool.setCurrentTryCount(FIX_CURRENTTRYCOUNT);
		flowPool.setDate(now);
		flowPool.setFlowPoolSourceType(FIX_FLOWPOOLSOURCETYPE);
		flowPool.setFlowPoolStatus(FIX_FLOWPOOLSTATUS);
		flowPool.setModifyDate(now);
		flowPool.setNextExecutionDate(now);
		flowPool.setNextNodeId(null);
		flowPool.setProcessExceptionId(null);
		return flowPool;
	}

	/**
	 * @param contextKey
	 * @param eventSourceId
	 * @param flowId
	 * @param flowInitEnvironment
	 * @return
	 */
	public static FlowPool createWithParameters(String contextKey,
			Long eventSourceId, Long flowId, String flowInitEnvironment) {
		FlowPool flowPool = createWithDefaults();
		flowPool.setContextKey(contextKey);
		flowPool.setEventSourceId(eventSourceId);
		flowPool.setFlowId(flowId);
		flowPool.setFlowInitEnvironment(flowInitEnvironment);
		return flowPool;
	}

}
