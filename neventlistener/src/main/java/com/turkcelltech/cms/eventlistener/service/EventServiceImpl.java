/**
 *
 */
package com.turkcelltech.cms.eventlistener.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.turkcelltech.cms.eventlistener.domain.Event;
import com.turkcelltech.cms.eventlistener.persistence.FlowPoolRepository;

/**
 * @author TTDKOC
 */
@Service
public class EventServiceImpl implements EventService {
	private static final Logger logger = LoggerFactory
			.getLogger(EventServiceImpl.class);
	private static final Logger eventInLogger = LoggerFactory
			.getLogger("eventInLogger");
	private static final Logger eventSkipLogger = LoggerFactory
			.getLogger("eventSkipLogger");
	private static final Logger eventExceptionLogger = LoggerFactory
			.getLogger("eventExceptionLogger");

	@Autowired
	private FlowPoolRepository eventRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.turkcelltech.eventlistener.service.EventService#logSkippedEvent(com
	 * .turkcelltech.eventlistener.domain.Event, java.lang.Long,
	 * java.lang.String,
	 * com.turkcelltech.eventlistener.service.EventService.EventSkipType)
	 */
	@Override
	public void logSkippedEvent(Event event, Integer eventId,
			Integer eventChannel, EventSkipType eventSkipType) {
		logger.debug(
				"Skipped Event:{}, eventId:{}, eventChannel:{}, eventSkipType: {}",
				new Object[] { event, eventId, eventChannel, eventSkipType });
		eventSkipLogger.info("{} {} {} {}", new Object[] { event.toString(),
				eventId.toString(), eventChannel, eventSkipType });
	}

	@Override
	public void logAsIncommingEvent(Event event, Integer eventId,
			Integer eventChannel) {
		logger.debug("Incomming Event:{}, eventId:{}, eventChannel:{}",
				new Object[] { event, eventId, eventChannel });
		eventInLogger.info("{} {} {}",
				new Object[] { event.toString(), eventId.toString(),
						eventChannel });
	}

	@Override
	public void logAsException(Event event, Integer eventId,
			Integer eventChannel, RuntimeException e) {
		eventExceptionLogger.info("{} {} {}", new Object[] { event.toString(),
				eventId.toString(), eventChannel, e });
	}

}
