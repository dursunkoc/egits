/**
 *
 */
package com.turkcelltech.cms.eventlistener.service;

import static com.turkcelltech.cms.eventlistener.config.Config.EVENT_HANDLER_THREAD_POOL_SIZE;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.aric.myel.statements.components.Environment;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.turkcelltech.cms.eventlistener.domain.Event;
import com.turkcelltech.cms.eventlistener.domain.EventAttributeWithValue;
import com.turkcelltech.cms.eventlistener.domain.EventHandler;
import com.turkcelltech.cms.eventlistener.domain.FlowPool;
import com.turkcelltech.cms.eventlistener.util.EventHandlerCall;

/**
 * @author TTDKOC
 */
@Service
public class EventHandlerServiceThreadPoolImpl implements EventHandlerService {
	private static final ListeningExecutorService EXECUTOR = MoreExecutors
			.listeningDecorator(Executors
					.newFixedThreadPool(EVENT_HANDLER_THREAD_POOL_SIZE));
	private static final Logger logger = LoggerFactory
			.getLogger(EventHandlerServiceThreadPoolImpl.class);

	/**
	 * @param eventHandler
	 * @param event
	 * @param eventId
	 * @param eventChannel
	 * @return
	 */
	@Override
	public ListenableFuture<FlowPool> handle(final EventHandler eventHandler,
			final Event event,
			final List<EventAttributeWithValue> attributeWithValues,
			final Environment env) {
		Callable<FlowPool> handleCall = new EventHandlerCall(eventHandler,
				event, attributeWithValues, env);
		if (logger.isDebugEnabled()) {
			logger.debug("handleCall submitted: {}", handleCall.toString());
		}
		return EXECUTOR.submit(handleCall);
	}
}
