/**
 *
 */
package com.turkcelltech.cms.eventlistener.service;

import java.util.List;
import java.util.concurrent.Future;

import com.aric.myel.statements.components.Environment;
import com.turkcelltech.cms.eventlistener.domain.Event;
import com.turkcelltech.cms.eventlistener.domain.EventAttributeWithValue;
import com.turkcelltech.cms.eventlistener.domain.EventHandler;
import com.turkcelltech.cms.eventlistener.domain.FlowPool;

/**
 * @author TTDKOC
 */
public interface EventHandlerService {
	/**
	 * @param eventHandler
	 * @param event
	 * @param attributeWithValues
	 * @param env
	 * @return
	 */
	public Future<FlowPool> handle(EventHandler eventHandler, Event event,
			List<EventAttributeWithValue> attributeWithValues, Environment env);
}
