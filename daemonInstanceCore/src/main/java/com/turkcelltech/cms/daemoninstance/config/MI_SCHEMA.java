/**
 * 
 */
package com.turkcelltech.cms.daemoninstance.config;
import static com.turkcelltech.cms.daemoninstance.config.Config.DB_SCHEMA_CHANNEL;
import static com.turkcelltech.cms.daemoninstance.config.Config.DB_SCHEMA_CORE;
import static com.turkcelltech.cms.daemoninstance.config.Config.DB_SCHEMA_EVENT_DRIVEN;
import static com.turkcelltech.cms.daemoninstance.config.Config.DB_SCHEMA_FLOW_ENGINE;
import static com.turkcelltech.cms.daemoninstance.config.Config.DB_SCHEMA_FULFILLMENT;
import static com.turkcelltech.cms.daemoninstance.config.Config.DB_SCHEMA_MESSAGE_DRIVEN;
import static com.turkcelltech.cms.daemoninstance.config.Config.MI_SCHEMA_CH;
import static com.turkcelltech.cms.daemoninstance.config.Config.MI_SCHEMA_CORE;
import static com.turkcelltech.cms.daemoninstance.config.Config.MI_SCHEMA_EDIB;
import static com.turkcelltech.cms.daemoninstance.config.Config.MI_SCHEMA_FE;
import static com.turkcelltech.cms.daemoninstance.config.Config.MI_SCHEMA_FFM;
import static com.turkcelltech.cms.daemoninstance.config.Config.MI_SCHEMA_MDIB;
/**
 * @author TTDKOC
 *
 */
public enum MI_SCHEMA {
	CORE(MI_SCHEMA_CORE, DB_SCHEMA_CORE), MDIB(MI_SCHEMA_MDIB,
			DB_SCHEMA_MESSAGE_DRIVEN), EDIB(MI_SCHEMA_EDIB,
			DB_SCHEMA_EVENT_DRIVEN), FFM(MI_SCHEMA_FFM, DB_SCHEMA_FULFILLMENT), CH(
			MI_SCHEMA_CH, DB_SCHEMA_CHANNEL), FE(MI_SCHEMA_FE,
			DB_SCHEMA_FLOW_ENGINE);
	public final String realVal;
	public final String defaultVal;

	private MI_SCHEMA(String realVal, String defaultVal) {
		this.defaultVal = defaultVal;
		this.realVal = realVal;
	}

}

