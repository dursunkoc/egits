/**
 *
 */
package com.turkcelltech.cms.eventlistener.domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.google.common.base.Objects;

/**
 * @author TTDKOC
 */
public class EventType implements RowMapper<EventType>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6973767409827152458L;
	public static final String QUERY_SELECT_ALL = "SELECT et.NEVENT_TYPE_ID as id,et.NEVENT_ID as eventId,et.TEVENT_TYPE_NAME as name,et.CEVENTAGENT as eventAgent,et.CEVENTCRITERIA as eventCriteria from COMET_EDIB.TEVENT_TYPE et";
	public static final String JOIN_TCODE = " JOIN COMET_CORE.TCODE c on C.TCODE = 'CEVENTAGENT' and C.NVALUE=et.CEVENTAGENT";
	public static final String QUERY_SELECT_ALL_BY_EVENT_ID_AND_EVENT_CHANNEL = QUERY_SELECT_ALL
			+ JOIN_TCODE
			+ " WHERE et.NEVENT_ID = :eventId AND c.TDESC = :eventAgent";
	private final Long id; // eventTypeId
	private final Integer eventId; // eventId
	private final String name; // eventTypeName
	private final Integer eventAgent; // eventAgent
	private final String eventCriteria;// eventCriteria

	public EventType() {
		this(null, null, null, null, null);
	}

	public EventType(Long id, Integer eventId, String name, Integer eventAgent,
			String eventCriteria) {
		this.id = id;
		this.eventId = eventId;
		this.name = name;
		this.eventAgent = eventAgent;
		this.eventCriteria = eventCriteria;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id)
				.add("eventId", eventId).add("name", name)
				.add("eventAgent", eventAgent)
				.add("eventCriteria", eventCriteria).toString();
	}

	public Long getId() {
		return id;
	}

	public Integer getEventId() {
		return eventId;
	}

	public String getName() {
		return name;
	}

	public Integer getEventAgent() {
		return eventAgent;
	}

	public String getEventCriteria() {
		return eventCriteria;
	}

	public static RowMapper<EventType> createRowMapper() {
		return new EventType();
	}

	@Override
	public EventType mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EventType(rs.getLong("id"), rs.getInt("eventId"),
				rs.getString("name"), rs.getInt("eventAgent"),
				rs.getString("eventCriteria"));
	}
}
