package com.turkcelltech.cms.eventlistener.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.turkcelltech.cms.daemoninstance.config.Config;
import com.turkcelltech.cms.daemoninstance.config.MI_SCHEMA;
import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance;
import com.turkcelltech.cms.daemoninstance.persistance.DaemonInstanceRepository;
import com.turkcelltech.cms.daemoninstance.util.DaemonInstanceUtil;
import com.turkcelltech.cms.eventlistener.domain.FlowPool;
import com.turkcelltech.cms.utils.ArrayUtil;

/**
 * Created with IntelliJ IDEA. User: TTDKOC Date: 10/10/13 Time: 10:21 PM To
 * change this template use File | Settings | File Templates.
 */
@Repository
public class FlowPoolRepositoryJdbcImpl implements FlowPoolRepository {
	private static final Logger logger = LoggerFactory
			.getLogger(FlowPoolRepositoryJdbcImpl.class);
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDatasource(DataSource datasource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(datasource);
	}

	@Autowired
	private DaemonInstanceRepository daemonInstanceRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.turkcelltech.cms.eventlistener.persistence.FlowPoolRepository#
	 * insertFlowPools
	 * (com.turkcelltech.cms.daemoninstance.domain.DaemonInstance,
	 * com.turkcelltech.cms.eventlistener.domain.FlowPool[])
	 */
	@Override
	@Transactional
	public void insertFlowPools(
			final DaemonInstance daemonInstanceForEventType,
			final FlowPool[] flowPools) {
		final FlowPool[] flowPoolsToUse = ArrayUtil.dropNullItems(flowPools,
				FlowPool.class);

		if (flowPoolsToUse == null || flowPoolsToUse.length == 0) {
			logger.debug("Nothing to insert");
			return;
		}

		int nrOfItems = flowPoolsToUse.length;
		logger.debug("Will insert {} records", nrOfItems);
		Map<Long, List<FlowPool>> partitionedFlowPools = partitionFlowPoolByFlowId(flowPoolsToUse);
		for (Entry<Long, List<FlowPool>> partFlowPools : partitionedFlowPools
				.entrySet()) {
			SqlParameterSource[] batchArgs = SqlParameterSourceUtils
					.createBatch(partFlowPools.getValue().toArray());
			String schemaName = findSchemaName(daemonInstanceForEventType,
					partFlowPools);

			String sqlToUse = FlowPool.INSERT_SQL.replace(Config.SCHEMA_HOLDER,
					schemaName);
			jdbcTemplate.batchUpdate(sqlToUse, batchArgs);
		}
		logger.debug("Successfully inserted {} records", nrOfItems);
	}

	private String findSchemaName(
			final DaemonInstance daemonInstanceForEventType,
			Entry<Long, List<FlowPool>> partFlowPools) {
		DaemonInstance daemonInstanceForFlow = daemonInstanceRepository
				.getDaemonInstanceFor(Config.FLOW_INSTANCE_TYPE_ID,
						partFlowPools.getKey().toString());
		String schemaName;
		if (daemonInstanceForFlow.equals(daemonInstanceRepository
				.getDaemonCurrentInstance())) {
			schemaName = DaemonInstanceUtil.getSchemaName(
					daemonInstanceForEventType, MI_SCHEMA.FE);
		} else {
			schemaName = DaemonInstanceUtil.getSchemaName(
					daemonInstanceForFlow, MI_SCHEMA.FE);
		}
		return schemaName;
	}

	/**
	 * @param flowPoolsToUse
	 * @return
	 */
	private Map<Long, List<FlowPool>> partitionFlowPoolByFlowId(
			final FlowPool[] flowPoolsToUse) {
		Map<Long, List<FlowPool>> partitionedFlowPools = new HashMap<Long, List<FlowPool>>();
		for (FlowPool flowPool : flowPoolsToUse) {
			List<FlowPool> part = partitionedFlowPools
					.get(flowPool.getFlowId());
			if (part == null) {
				part = new ArrayList<FlowPool>();
				part.add(flowPool);
				partitionedFlowPools.put(flowPool.getFlowId(), part);
			} else {
				part.add(flowPool);
			}
		}
		return partitionedFlowPools;
	}

}
