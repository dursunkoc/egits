/**
 * 
 */
package com.turkcelltech.cms.eventlistener.domain.jsonmapper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;


/**
 * @author Dursun KOC
 * 
 */

public class CustomObjectMapper extends ObjectMapper {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2504910508999063718L;

	public CustomObjectMapper() {
        configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, true);            
        configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, true);
        setDateFormat(new ISO8601DateFormat());
    }
}
