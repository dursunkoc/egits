package com.turkcelltech.cms.eventlistener.persistence;

import java.util.List;

import com.turkcelltech.cms.eventlistener.domain.EventHandler;

/**
 * Created with IntelliJ IDEA. User: TTDKOC Date: 10/12/13 Time: 4:23 PM To
 * change this template use File | Settings | File Templates.
 */
public interface EventHandlerRepository {
	/**
	 * @param eventTypeId
	 * @return
	 */
	public List<EventHandler> getActiveEventHandlersForEventTypeId(
			Long eventTypeId);
}
