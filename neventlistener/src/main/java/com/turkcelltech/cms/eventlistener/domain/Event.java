/**
 *
 */
package com.turkcelltech.cms.eventlistener.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.google.common.base.Objects;
import com.turkcelltech.cms.eventlistener.domain.xmladapter.JaxbDateAdapter;
import com.turkcelltech.cms.eventlistener.domain.xmladapter.MapStrStrAdapter;

/**
 * @author Dursun KOC
 */
@XmlType
public final class Event implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6981395913274215995L;
	@XmlElement
    private Long eventSourceId;
    @XmlElement
    private String contextKey;
    @XmlJavaTypeAdapter(JaxbDateAdapter.class)
    @XmlElement
    private Date date;
    @XmlJavaTypeAdapter(MapStrStrAdapter.class)
    @XmlElement(name = "parameters")
    private Map<String, String> parameters;

    public Long getEventSourceId() {
        return this.eventSourceId;
    }

    public Date getDate() {
        return date;
    }

    public String getContextKey() {
        return contextKey;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("eventSourceId", eventSourceId)
                .add("contextKey", contextKey)
                .add("date", date)
                .add("parameters", parameters)
                .toString();
    }

}
