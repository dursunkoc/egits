package com.turkcelltech.cms.eventlistener.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.aric.myel.statements.Statement;
import com.aric.myel.statements.StatementResult;
import com.aric.myel.statements.StatementUtils;
import com.aric.myel.statements.components.Environment;
import com.aric.myel.statements.components.ReturnValue;
import com.aric.myel.statements.components.TYPE;
import com.turkcelltech.cms.eventlistener.config.Config;
import com.turkcelltech.cms.eventlistener.domain.Event;
import com.turkcelltech.cms.eventlistener.domain.EventAttribute;
import com.turkcelltech.cms.eventlistener.domain.EventAttributeWithValue;
import com.turkcelltech.cms.eventlistener.domain.EventHandler;
import com.turkcelltech.cms.eventlistener.domain.EventType;

public final class EventUtil {
	public static final String VALID_MESSAGE = "";

	public static class ValidationResult {
		public final String result;
		public final List<EventAttributeWithValue> attributeWithValues;

		public ValidationResult(String result,
				List<EventAttributeWithValue> attributeWithValues) {
			this.result = result;
			this.attributeWithValues = attributeWithValues;
		}

		public boolean isValid() {
			return EventUtil.VALID_MESSAGE.equals(this.result);
		}
	}

	private EventUtil() {
	}

	/**
	 * @param eventType
	 * @param eventAttributes
	 * @param parameters
	 * @return
	 */
	public static ValidationResult validateParameters(EventType eventType,
			List<EventAttribute> eventAttributes, Map<String, String> parameters) {
		if (eventType == null) {
			return new ValidationResult("Event Type is not valid!",
					Collections.<EventAttributeWithValue> emptyList());
		}
		if (eventAttributes.size() != parameters.size()) {
			return new ValidationResult(
					"Number of event attributes does not match the event definition.(#sentEventAttributes='"
							+ parameters.size()
							+ "'; #expectedEventAttributes='"
							+ eventAttributes.size() + "')",
					Collections.<EventAttributeWithValue> emptyList());
		}
		List<EventAttributeWithValue> attributeWithValues = new ArrayList<EventAttributeWithValue>(
				eventAttributes.size());
		for (EventAttribute eventAttribute : eventAttributes) {
			if (!parameters.containsKey(eventAttribute.getName())) {
				return new ValidationResult("Event Type Attribute '"
						+ eventAttribute.getName() + "' does not exists.",
						Collections.<EventAttributeWithValue> emptyList());
			} else {
				attributeWithValues.add(new EventAttributeWithValue(
						eventAttribute,
						parameters.get(eventAttribute.getName())));
			}
		}
		return new ValidationResult(VALID_MESSAGE, attributeWithValues);
	}

	/**
	 * @param et
	 * @param e
	 * @param attributeWithValues
	 * @return
	 */
	public static boolean eventCriteriaSatisfied(EventType et, Environment env) {
		Statement stmt = StatementUtils.string2Statement(et.getEventCriteria());
		return executeCriteriaInternal(env, stmt);
	}

	public static Environment generateEnvironment(Event e,
			List<EventAttributeWithValue> attributeWithValues) {
		Environment environment = new Environment();

		ReturnValue rvContextKey = new ReturnValue(e.getContextKey(),
				TYPE.STRING);
		environment.addOrUpdateVariable("contextKey", rvContextKey);

		for (EventAttributeWithValue attrWithVal : attributeWithValues) {
			EventAttribute eventAttribute = attrWithVal.getEventAttribute();
			String value = attrWithVal.getValue();
			ReturnValue rv = eventAttributeToReturnValue(eventAttribute, value);
			environment.addOrUpdateVariable(eventAttribute.getName(), rv);
		}
		return environment;
	}

	private static ReturnValue eventAttributeToReturnValue(
			EventAttribute eventAttribute, String value) {
		TYPE type = TYPE.getType(eventAttribute.getType());
		if (type.equals(TYPE.DATE)) {
			return new ReturnValue(new Date(Long.parseLong(value)), type,
					Config.SYSTEM_DATE_FORMAT_LONG);
		}
		return new ReturnValue(value, type);
	}

	public static boolean handlerCriteriaSatisfied(EventHandler eh,
			Event event, Environment env) {
		Statement stmt = StatementUtils.string2Statement(eh
				.getStatementCriteria());
		return executeCriteriaInternal(env, stmt);
	}

	private static boolean executeCriteriaInternal(Environment env,
			Statement stmt) {
		if (stmt.isEmpty()) {
			return true;
		}
		StatementResult stmtResult = stmt.execute(env);
		return stmtResult.isSuccessful();
	}

	public static String createFlowInitEnvironment(Event event,
			EventHandler eventHandler,
			List<EventAttributeWithValue> attributeWithValues) {
		final StringBuffer environmentBuffer = new StringBuffer();

		final Long eventTypeId = eventHandler.getEventTypeId();

		appendEventAttributes(attributeWithValues, eventTypeId,
				environmentBuffer);

		appendEventTypeId(eventTypeId, environmentBuffer);

		appendCampaignXId(eventHandler, environmentBuffer);

		return environmentBuffer.toString();
	}

	private static void appendEventAttributes(
			List<EventAttributeWithValue> attributeWithValues,
			Long eventTypeId, StringBuffer environmentBuffer) {
		for (EventAttributeWithValue attrWithVal : attributeWithValues) {

			EventAttribute eventAttribute = attrWithVal.getEventAttribute();
			final Integer attributeType = eventAttribute.getType();
			environmentBuffer.append(Config.ENV_NAME_EVENT_ATTRIBUTE_PREFIX);
			environmentBuffer.append(eventAttribute.getId());
			environmentBuffer.append("(").append(attributeType).append(")");
			environmentBuffer.append("=");
			environmentBuffer.append(attrWithVal.getValue());
			environmentBuffer.append(Config._MAJORDELIMITER);
		}
	}

	private static void appendEventTypeId(final Long eventTypeId,
			final StringBuffer environmentBuffer) {
		environmentBuffer
				.append("eventTypeId(" + TYPE.NUMERIC.getTypeId() + ")")
				.append("=").append(eventTypeId);
		environmentBuffer.append(Config._MAJORDELIMITER);
	}

	private static void appendCampaignXId(EventHandler eh,
			StringBuffer environmentBuffer) {
		if (eh.getCampaignId() != null) {
			environmentBuffer.append(Config.WS_JSON_PARAMETER_CAMPAIGNID);
			environmentBuffer.append("(").append(TYPE.NUMERIC.getTypeId())
					.append(")");
			environmentBuffer.append("=");
			environmentBuffer.append(eh.getCampaignId());
			environmentBuffer.append(Config._MAJORDELIMITER.toString());
		}
		if (eh.getCampaignGroupId() != null) {
			environmentBuffer.append(Config.WS_JSON_PARAMETER_CAMPAIGNGROUPID);
			environmentBuffer.append("(").append(TYPE.NUMERIC.getTypeId())
					.append(")");
			environmentBuffer.append("=");
			environmentBuffer.append(eh.getCampaignGroupId());
			environmentBuffer.append(Config._MAJORDELIMITER.toString());
		}
	}
}
