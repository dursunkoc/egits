/**
 * 
 */
package com.turkcelltech.cms.eventlistener.persistence;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.turkcelltech.cms.eventlistener.domain.EventAttribute;
import com.turkcelltech.cms.eventlistener.domain.EventType;

/**
 * @author TTDKOC
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testConfiguration.xml")
public class EventTypeRepositoryJdbcImplTest {

	@Autowired
	private EventTypeRepository eventTypeRepository;
	/**
	 * Test method for {@link com.turkcelltech.cms.eventlistener.persistence.EventTypeRepositoryJdbcImpl#getEventType(java.lang.Long, java.lang.String)}.
	 */
	@Test
	public void testGetEventType() {
		EventType eventType = eventTypeRepository.getEventType(8787l, "BILLING");
		Assert.assertNotNull("Event Type should exists!", eventType);
	}

	@Test
	public void testGetEventTypeParameters() throws Exception {
		EventType eventType = eventTypeRepository
				.getEventType(8787l, "BILLING");
		List<EventAttribute> eventTypeParameters = eventTypeRepository
				.getEventTypeParameters(eventType);
		Assert.assertFalse(
				"There has to be atleast one eventType Parameter defined.",
				eventTypeParameters.isEmpty());
	}

}
