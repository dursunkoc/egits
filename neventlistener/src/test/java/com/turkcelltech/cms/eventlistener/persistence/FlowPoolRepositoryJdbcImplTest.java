/**
 * 
 */
package com.turkcelltech.cms.eventlistener.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance;
import com.turkcelltech.cms.daemoninstance.persistance.DaemonInstanceRepository;
import com.turkcelltech.cms.eventlistener.domain.FlowPool;

/**
 * @author TTDKOC
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testConfiguration.xml")
public class FlowPoolRepositoryJdbcImplTest {

	@Autowired
	private FlowPoolRepository flowPoolRepository;
	@Autowired
	private DaemonInstanceRepository daemonInstanceRepository;

	/**
	 * Test method for {@link com.turkcelltech.cms.eventlistener.persistence.FlowPoolRepositoryJdbcImpl#insertFlowPools(com.turkcelltech.cms.eventlistener.domain.FlowPool[])}.
	 */
	@Test
	public void testInsertFlowPools() {
		FlowPool[] flowPools = new FlowPool[1];
		FlowPool flowPool = FlowPool.createWithParameters("555", -1l, -1l,
				"initEnvTest");
		flowPools[0] = flowPool;
		DaemonInstance daemonCurrentInstance = daemonInstanceRepository
				.getDaemonCurrentInstance();
		FlowPool.INSERT_SQL = FlowPool.INSERT_SQL.replace(
				"COMET_FE.SEQ_NFLOWPOOL_ID.nextval",
				"NEXT VALUE FOR COMET_FE.SEQ_NFLOWPOOL_ID");
		flowPoolRepository.insertFlowPools(daemonCurrentInstance, flowPools);
	}

	/**
	 * Test method for
	 * {@link com.turkcelltech.cms.eventlistener.persistence.FlowPoolRepositoryJdbcImpl#insertFlowPools(com.turkcelltech.cms.eventlistener.domain.FlowPool[])}
	 * .
	 */
	@Test
	public void testInsertFlowPoolsWithNullElements() {
		FlowPool[] flowPools = new FlowPool[2];
		FlowPool flowPool = FlowPool.createWithParameters("555", -1l, -1l,
				"initEnvTest");
		flowPools[0] = flowPool;
		FlowPool flowPool2 = null;
		flowPools[1] = flowPool2;
		DaemonInstance daemonCurrentInstance = daemonInstanceRepository
				.getDaemonCurrentInstance();
		FlowPool.INSERT_SQL = FlowPool.INSERT_SQL.replace(
				"COMET_FE.SEQ_NFLOWPOOL_ID.nextval",
				"NEXT VALUE FOR COMET_FE.SEQ_NFLOWPOOL_ID");
		flowPoolRepository.insertFlowPools(daemonCurrentInstance, flowPools);
	}

}
