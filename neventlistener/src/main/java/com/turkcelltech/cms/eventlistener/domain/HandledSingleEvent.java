package com.turkcelltech.cms.eventlistener.domain;

import java.io.Serializable;

import com.google.common.base.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: TTDKOC
 * Date: 10/10/13
 * Time: 5:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class HandledSingleEvent implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5556766312710084400L;
	private final PushedEvent pushedEvent;
    private final FlowPool[] flowPools;

    public HandledSingleEvent(PushedEvent pushedEvent, FlowPool[] flowPools) {
        this.pushedEvent = pushedEvent;
        this.flowPools = flowPools;
    }

    public PushedEvent getPushedEvent() {
        return pushedEvent;
    }

    public FlowPool[] getFlowPools() {
        return flowPools;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this).add("pushedEvent", pushedEvent).add("flowPools", flowPools).toString();
    }
}
