/**
 * 
 */
package com.turkcelltech.cms.eventlistener.domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.google.common.base.Objects;

/**
 * @author TTDKOC
 *
 */
public class EventAttribute implements RowMapper<EventAttribute>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4262419049728596012L;
	public static final String QUERY_SELECT_ALL = "SELECT  NEVENT_ATTRIBUTE_ID as id, NEVENT_TYPE_ID as eventTypeId, TATTRIBUTE_NAME as name, CATTRIBUTETYPE as type from COMET_EDIB.TEVENT_ATTRIBUTE";
	public static final String QUERY_SELECT_ALL_BY_EVENT_TYPE_ID = QUERY_SELECT_ALL
			+ " WHERE NEVENT_TYPE_ID = :eventTypeId";
	private final Long id;
	private final Long eventTypeId;
	private final String name;
	private final Integer type;

	/**
	 * @param id
	 * @param eventTypeId
	 * @param name
	 * @param type
	 */
	public EventAttribute(Long id, Long eventTypeId, String name, Integer type) {
		this.id = id;
		this.eventTypeId = eventTypeId;
		this.name = name;
		this.type = type;
	}



	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the eventTypeId
	 */
	public Long getEventTypeId() {
		return eventTypeId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", this.id)
				.add("name", this.name).add("eventTypeId", this.eventTypeId)
				.add("type", this.type).toString();
	}

	@Override
	public EventAttribute mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EventAttribute(rs.getLong("id"), rs.getLong("eventTypeId"),
				rs.getString("name"), rs.getInt("type"));
	}

	public static RowMapper<EventAttribute> createRowMapper() {
		return new EventAttribute(null, null, null, null);
	}

}
