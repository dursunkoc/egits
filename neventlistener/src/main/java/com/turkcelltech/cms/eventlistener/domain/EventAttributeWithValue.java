/**
 * 
 */
package com.turkcelltech.cms.eventlistener.domain;

import java.io.Serializable;

import com.google.common.base.Objects;

/**
 * @author TTDKOC
 *
 */
public class EventAttributeWithValue implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3789816111031673889L;
	private final EventAttribute eventAttribute;
	private final String value;

	/**
	 * @param eventAttribute
	 * @param value
	 */
	public EventAttributeWithValue(EventAttribute eventAttribute, String value) {
		this.eventAttribute = eventAttribute;
		this.value = value;
	}

	/**
	 * @return the eventAttribute
	 */
	public EventAttribute getEventAttribute() {
		return eventAttribute;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("eventAttribute", eventAttribute).add("value", value)
				.toString();
	}

}
