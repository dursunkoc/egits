/**
 * 
 */
package com.turkcelltech.cms.eventlistener.persistence;

import java.util.List;

import com.turkcelltech.cms.eventlistener.domain.EventAttribute;
import com.turkcelltech.cms.eventlistener.domain.EventType;

/**
 * @author TTDKOC
 * 
 */
public interface EventTypeRepository {

	/**
	 * 
	 */
	public void flushEventTypeCache();

	/**
	 * 
	 */
	public void flushEventTypeParametersCache();

	/**
	 * @param eventId
	 * @param eventChannel
	 * @return
	 */
	public EventType getEventType(Long eventId, String eventChannel);

	/**
	 * @param eventType
	 * @return
	 */
	public List<EventAttribute> getEventTypeParameters(EventType eventType);



}
