/**
 * 
 */
package com.turkcelltech.cms.daemoninstance.persistance;

import static com.turkcelltech.cms.utils.BooleanUtil.Not;
import static com.turkcelltech.cms.utils.QueryUtil.oneAndOnlyOne;
import static com.turkcelltech.cms.utils.QueryUtil.onlyOneOrDefault;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.turkcelltech.cms.daemoninstance.config.Config;
import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance;
import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance.MEMBERSHIP;

/**
 * @author TTDKOC
 * 
 */
public class DaemonInstanceRepositoryJdbcImpl implements
		DaemonInstanceRepository {
	private static final Logger logger = LoggerFactory
			.getLogger(DaemonInstanceRepositoryJdbcImpl.class);
	private final NamedParameterJdbcTemplate jdbcTemplate;
	private final DaemonInstance daemonCurrentInstance;

	/**
	 * @param dataSource
	 * @param currentInstanceName
	 * @param daemonCurrentInstance
	 */
	public DaemonInstanceRepositoryJdbcImpl(DataSource dataSource,
			String currentInstanceName) {
		logger.debug(
				"Creating Daemon instance Repository for daemon instance name {}",
				currentInstanceName);
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		this.daemonCurrentInstance = this
				.getDaemonInstanceByName(currentInstanceName);
		logger.debug(
				"Created Daemon instance Repository for daemon instance {}",
				this.daemonCurrentInstance);
	}

	@Override
	@Cacheable("daemonInstance")
	public DaemonInstance getDaemonInstanceByName(String name) {
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("name", name);
		List<DaemonInstance> daemonInstances = jdbcTemplate.query(
				DaemonInstance.sqlForGetInstanceByName, paramMap,
				new DaemonInstance());
		return oneAndOnlyOne(daemonInstances, "getDaemonInstanceByName", name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.turkcelltech.comet.commonutil.services.persistence.DaemonInstanceDao
	 * #getDaemonCurrentInstance()
	 */
	@Override
	public DaemonInstance getDaemonCurrentInstance() {
		return daemonCurrentInstance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.turkcelltech.comet.commonutil.services.persistence.DaemonInstanceDao
	 * #getDaemonInstanceFor(java.lang.Long, java.lang.String,
	 * com.turkcelltech.comet.commonutil.model.DaemonInstance)
	 */
	@Override
	@Cacheable("daemonInstance")
	public DaemonInstance getDaemonInstanceFor(Long daemonInstanceTypeId,
			String value) {
		return getDaemonInstanceForInternal(daemonInstanceTypeId, value,
				this.getDaemonCurrentInstance());
	}

	/**
	 * @param daemonInstanceTypeId
	 * @param value
	 * @param defaultInstance
	 * @return
	 */
	private DaemonInstance getDaemonInstanceForInternal(
			Long daemonInstanceTypeId, String value,
			DaemonInstance defaultInstance) {
		DaemonInstance detachedDaemonInstance = getDetachedDaemonInstanceForInternal(
				daemonInstanceTypeId, value, defaultInstance);
		detachedDaemonInstance.setMembership(MEMBERSHIP.NON_MEMBER);
		if (defaultInstance == detachedDaemonInstance) {
			DaemonInstance grouppedDaemonInstance = getGrouppedDaemonInstanceForInternal(
					daemonInstanceTypeId, value, defaultInstance);
			return grouppedDaemonInstance;
		}

		return detachedDaemonInstance;
	}

	/**
	 * @param daemonInstanceTypeId
	 * @param value
	 * @param defaultInstance
	 * @return
	 */
	private DaemonInstance getDetachedDaemonInstanceForInternal(
			Long daemonInstanceTypeId, String value,
			DaemonInstance defaultInstance) {
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("value", value);
		paramMap.addValue("daemoninstancetype_id", daemonInstanceTypeId);
		List<DaemonInstance> daemonInstances = jdbcTemplate.query(
				DaemonInstance.sqlForGetInstanceFor, paramMap,
				new DaemonInstance());
		return onlyOneOrDefault(daemonInstances, defaultInstance,
				"getDetachedDaemonInstanceForInternal", value,
				daemonInstanceTypeId);
	}

	/**
	 * @param daemonInstanceTypeId
	 * @param value
	 * @param defaultInstance
	 * @return
	 */
	private DaemonInstance getGrouppedDaemonInstanceForInternal(
			Long daemonInstanceTypeId, String value,
			DaemonInstance defaultInstance) {

		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("value", value);
		paramMap.addValue("daemoninstancetype_id", daemonInstanceTypeId);
		paramMap.addValue("CPREFERED_INSTANCE",
				Config.CODE_PREFERED_INSTANCE_PREFERED);
		List<DaemonInstance> daemonInstances = jdbcTemplate.query(
				DaemonInstance.sqlForGetGrouppedDaemonInstanceFor, paramMap,
				DaemonInstance.createRowMapper());
		DaemonInstance onlyOneOrDefault = onlyOneOrDefault(daemonInstances,
				defaultInstance, "getGrouppedDaemonInstanceForInternal", value,
				daemonInstanceTypeId, Config.CODE_PREFERED_INSTANCE_PREFERED);
		if (Not(onlyOneOrDefault.equals(defaultInstance))) {
			onlyOneOrDefault.setMembership(MEMBERSHIP.MEMBER);
		}
		return onlyOneOrDefault;
	}

}
