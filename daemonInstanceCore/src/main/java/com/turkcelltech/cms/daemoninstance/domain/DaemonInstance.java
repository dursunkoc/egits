/**
 * 
 */
package com.turkcelltech.cms.daemoninstance.domain;

import static com.turkcelltech.cms.daemoninstance.config.Config.DB_SCHEMA_CORE;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.turkcelltech.cms.daemoninstance.config.Config;

/**
 * @author TTDKOC
 * 
 */
public class DaemonInstance implements RowMapper<DaemonInstance>,
		Comparable<DaemonInstance>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9036426191544545324L;
	private static final String DAEMONINSTANCE = "DAEMONINSTANCE";
	private static final String DAEMONINSTANCE_DECISIONVALUE = "DAEMONINSTANCE_DECISIONVALUE";
	private static final String DAEMONINSTANCE_GROUP = "DAEMONINSTANCE_GROUP";
	private static final String DAEMONINSTANCE_GROUP_MEMBERS = "DAEMONINSTANCE_GROUP_MEMBERS";
	private static final String selectHeaderForGetInstance = new StringBuffer()
			.append("SELECT i.id, i.name, i.schemaname, i.daemoninstancetype_id, i.cmsapplication_instance_id ")
			.toString();
	public static final String sqlForGetInstanceFor = new StringBuffer()
			.append(selectHeaderForGetInstance).append(" FROM ")
			.append(DB_SCHEMA_CORE).append(".").append(DAEMONINSTANCE)
			.append(" i, ").append(DB_SCHEMA_CORE).append(".")
			.append(DAEMONINSTANCE_DECISIONVALUE).append(" ID")
			.append(" WHERE i.id = id.daemoninstance_id")
			.append(" AND id.value = :value")
			.append(" AND id.daemoninstancetype_id = :daemoninstancetype_id ")
			.toString();
	public static final String sqlForGetGrouppedDaemonInstanceFor = new StringBuffer(
			selectHeaderForGetInstance).append(" FROM ").append(DB_SCHEMA_CORE)
			.append(".").append(DAEMONINSTANCE_DECISIONVALUE).append(" ID ")
			.append(" JOIN ").append(DB_SCHEMA_CORE).append(".")
			.append(DAEMONINSTANCE_GROUP)
			.append(" dig ON DIG.ID = ID.DAEMONINSTANCE_GROUP_ID  ")
			.append(" JOIN ").append(DB_SCHEMA_CORE).append(".")
			.append(DAEMONINSTANCE_GROUP_MEMBERS)
			.append(" digm ON DIG.ID = DIGM.DAEMONINSTANCE_GROUP_ID  ")
			.append(" JOIN ").append(DB_SCHEMA_CORE).append(".")
			.append(DAEMONINSTANCE)
			.append(" i ON I.ID=DIGM.DAEMONINSTANCE_ID ")
			.append(" WHERE  id.value = :value ")
			.append(" AND id.daemoninstancetype_id = :daemoninstancetype_id  ")
			.append(" AND DIGM.CPREFERED_INSTANCE = :CPREFERED_INSTANCE ")
			.toString();;
	public static final String sqlForGetInstanceByName = new StringBuffer(
			selectHeaderForGetInstance).append("  FROM ")
			.append(Config.DB_SCHEMA_CORE).append(".").append(DAEMONINSTANCE)
			.append(" i").append(" WHERE i.name = :name").toString();;

	public static enum MEMBERSHIP {
		MEMBER, NON_MEMBER, UNKNOWN;
	}

	private Long id;
	private String name;
	private String schemaName;
	private Long daemonInstanceTypeId;
	private Long cmsApplicationInstanceId;
	private MEMBERSHIP membership;

	/**
	 * 
	 */
	public DaemonInstance() {
		this.membership = MEMBERSHIP.UNKNOWN;
	}

	/**
	 * @param id
	 * @param name
	 * @param schemaName
	 * @param daemonInstanceTypeId
	 * @param cmsApplicationInstanceId
	 */
	public DaemonInstance(Long id, String name, String schemaName,
			Long daemonInstanceTypeId, Long cmsApplicationInstanceId) {
		this.id = id;
		this.name = name;
		this.schemaName = schemaName;
		this.daemonInstanceTypeId = daemonInstanceTypeId;
		this.cmsApplicationInstanceId = cmsApplicationInstanceId;
		this.membership = MEMBERSHIP.UNKNOWN;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the schemaName
	 */
	public String getSchemaName() {
		return schemaName;
	}

	/**
	 * @param schemaName
	 *            the schemaName to set
	 */
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	/**
	 * @return the daemonInstanceTypeId
	 */
	public Long getDaemonInstanceTypeId() {
		return daemonInstanceTypeId;
	}

	/**
	 * @param daemonInstanceTypeId
	 *            the daemonInstanceTypeId to set
	 */
	public void setDaemonInstanceTypeId(Long daemonInstanceTypeId) {
		this.daemonInstanceTypeId = daemonInstanceTypeId;
	}

	/**
	 * @return
	 */
	public Long getCmsApplicationInstanceId() {
		return cmsApplicationInstanceId;
	}

	/**
	 * @param cmsApplicationInstanceId
	 */
	public void setCmsApplicationInstanceId(Long cmsApplicationInstanceId) {
		this.cmsApplicationInstanceId = cmsApplicationInstanceId;
	}

	/**
	 * @return
	 */
	public MEMBERSHIP getMembership() {
		return membership;
	}

	/**
	 * @param membership
	 */
	public void setMembership(MEMBERSHIP membership) {
		this.membership = membership;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */
	@Override
	public DaemonInstance mapRow(ResultSet resultSet, int rowNum)
			throws SQLException {
		DaemonInstance daemonInstance = new DaemonInstance(
				resultSet.getLong("id"), resultSet.getString("name"),
				resultSet.getString("schemaName"),
				resultSet.getLong("daemoninstanceType_Id"),
				resultSet.getLong("cmsapplication_instance_id"));
		return daemonInstance;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DaemonInstance (");
		builder.append("id: ").append(id);
		builder.append(", name: ").append(name);
		builder.append(", schemaName: ").append(schemaName);
		builder.append(", daemonInstanceTypeId: ").append(daemonInstanceTypeId);
		builder.append(", membership: ").append(this.membership.name());

		builder.append(")");
		return builder.toString();
	}

	@Override
	public int compareTo(DaemonInstance otherDeamonInstanceObject) {
		return (int) (this.getId().longValue() - otherDeamonInstanceObject
				.getId().longValue());
	}

	public static RowMapper<DaemonInstance> createRowMapper() {
		return new DaemonInstance();
	}

}
