/**
 * 
 */
package com.turkcelltech.cms.eventlistener.domain;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.google.common.base.Objects;

/**
 * @author TTDKOC
 * 
 */
public class EventHandler implements RowMapper<EventHandler>,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8416024601311884397L;

	public static final String QUERY_SELECT_ALL = new StringBuffer(
			"SELECT eh.NEVENT_HANDLER_ID AS id, eh.NFLOWID AS flowId, eh.NEVENT_TYPE_ID AS eventTypeId, eh.NCAMPAIGNDEFINITION_ID AS campaignDefinitionId, eh.CENABLED AS enabled, eh.CSTATEMENTCRITERIA AS statementcriteria, ")
			.append("CD.NCAMPAIGN_ID as campaignId, CD.NCAMPAIGNGROUP_ID as campaignGroupId ")
			.append("FROM COMET_EDIB.TEVENT_HANDLER eh ")
			.append("LEFT JOIN COMET_CORE.TCAMPAIGNDEFINITION cd on EH.NCAMPAIGNDEFINITION_ID = CD.NCAMPAIGNDEFINITION_ID ")
			.toString();

	public static final String QUERY_SELECT_ALL_BY_EVENT_TYPE_ID_AND_ACTIVE = new StringBuffer(
			QUERY_SELECT_ALL)
			.append(" WHERE eh.NEVENT_TYPE_ID = :eventTypeId")
			.append(" AND eh.CENABLED = :enabled")
			.append(" AND (eh.NCAMPAIGNDEFINITION_ID IS NULL")
			.append(" OR (CD.NCAMPAIGNDEFINITION_ID < 1000")
			.append(" OR (CD.CCAMPAIGNSTATUS = :campaignStatusScheduled")
			.append(" AND CD.NPROCESS_PRIORITY >= 0 AND SYSDATE BETWEEN CD.DSTARTDATE AND CD.DENDDATE")
			.append(")))").toString();
	private Long id;
	private Long flowId;
	private Long eventTypeId;
	private Long campaignDefinitionId;
	private Integer enabled;
	private String statementCriteria;
	private Long campaignId;
	private Long campaignGroupId;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the flowId
	 */
	public Long getFlowId() {
		return flowId;
	}

	/**
	 * @param flowId
	 *            the flowId to set
	 */
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	/**
	 * @return the eventTypeId
	 */
	public Long getEventTypeId() {
		return eventTypeId;
	}

	/**
	 * @param eventTypeId
	 *            the eventTypeId to set
	 */
	public void setEventTypeId(Long eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	/**
	 * @return the campaignDefinitionId
	 */
	public Long getCampaignDefinitionId() {
		return campaignDefinitionId;
	}

	/**
	 * @param campaignDefinitionId
	 *            the campaignDefinitionId to set
	 */
	public void setCampaignDefinitionId(Long campaignDefinitionId) {
		this.campaignDefinitionId = campaignDefinitionId;
	}

	/**
	 * @return the enabled
	 */
	public Integer getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 *            the enabled to set
	 */
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the statementCriteria
	 */
	public String getStatementCriteria() {
		return statementCriteria;
	}

	/**
	 * @param statementCriteria
	 *            the statementCriteria to set
	 */
	public void setStatementCriteria(String statementCriteria) {
		this.statementCriteria = statementCriteria;
	}

	/**
	 * @return the campaignId
	 */
	public Long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId
	 *            the campaignId to set
	 */
	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the campaignGroupId
	 */
	public Long getCampaignGroupId() {
		return campaignGroupId;
	}

	/**
	 * @param campaignGroupId
	 *            the campaignGroupId to set
	 */
	public void setCampaignGroupId(Long campaignGroupId) {
		this.campaignGroupId = campaignGroupId;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("flowId", flowId)
				.add("eventTypeId", eventTypeId)
				.add("campaignDefinitionId", campaignDefinitionId)
				.add("enabled", enabled)
				.add("statementCriteria", statementCriteria)
				.add("campaignId", campaignId)
				.add("campaignGroupId", campaignGroupId).toString();
	}

	/**
	 * @param id
	 * @param flowId
	 * @param eventTypeId
	 * @param campaignDefinitionId
	 * @param enabled
	 * @param statementCriteria
	 */
	public EventHandler(Long id, Long flowId, Long eventTypeId,
			Long campaignDefinitionId, Integer enabled,
			String statementCriteria, Long campaignId, Long campaignGroupId) {
		this.id = id;
		this.flowId = flowId;
		this.eventTypeId = eventTypeId;
		this.campaignDefinitionId = campaignDefinitionId;
		this.enabled = enabled;
		this.statementCriteria = statementCriteria;
		this.campaignId = campaignId;
		this.campaignGroupId = campaignGroupId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */
	@Override
	public EventHandler mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EventHandler(rs.getLong("id"), rs.getLong("flowId"),
				rs.getLong("eventTypeId"), rs.getLong("campaignDefinitionId"),
				rs.getInt("enabled"), rs.getString("statementCriteria"),
				rs.getLong("campaignId"), rs.getLong("campaignGroupId"));
	}

	/**
	 * @return
	 */
	public static RowMapper<EventHandler> createRowMapper() {
		return new EventHandler(null, null, null, null, null, null, null, null);
	}
}
