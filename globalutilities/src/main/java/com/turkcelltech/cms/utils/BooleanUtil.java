/**
 * 
 */
package com.turkcelltech.cms.utils;

/**
 * @author TTDKOC
 *
 */
public class BooleanUtil {
	private BooleanUtil() {
	}

	/**
	 * @param b
	 * @return
	 */
	public static boolean Not(boolean b) {
		return not(b);
	}

	public static boolean not(boolean b) {
		return !b;
	}
}
