package com.turkcelltech.cms.daemoninstance.config;

public class Config {
	public static final Object CODE_PREFERED_INSTANCE_PREFERED = 2;
	public static final Object CODE_PREFERED_INSTANCE_NOT_PREFERED = 1;

	public static final String DB_SCHEMA_SECURITY = "COMET_SECURITY";
	public static final String DB_SCHEMA_MART = "COMET_MART";
	public static final String DB_SCHEMA_OPERATION = "COMET_OP";
	public static final String DB_SCHEMA_SCHEDULER = "COMET_SCH";
	public static final String DB_SCHEMA_CORE = "COMET_CORE";
	public static final String DB_SCHEMA_FLOW_ENGINE = "COMET_FE";
	public static final String DB_SCHEMA_OUTBOUND = "COMET_OB";
	public static final String DB_SCHEMA_INBOUND = "COMET_IB";
	public static final String DB_SCHEMA_MESSAGE_DRIVEN = "COMET_MDIB";
	public static final String DB_SCHEMA_EVENT_DRIVEN = "COMET_EDIB";
	public static final String DB_SCHEMA_FULFILLMENT = "COMET_FFM";
	public static final String DB_SCHEMA_CHANNEL = "COMET_CH";
	public static final String MI_SCHEMA_CORE = "$" + DB_SCHEMA_CORE;
	public static final String MI_SCHEMA_CH = "$" + DB_SCHEMA_CHANNEL;
	public static final String MI_SCHEMA_EDIB = "$" + DB_SCHEMA_EVENT_DRIVEN;
	public static final String MI_SCHEMA_FFM = "$" + DB_SCHEMA_FULFILLMENT;
	public static final String MI_SCHEMA_MDIB = "$" + DB_SCHEMA_MESSAGE_DRIVEN;
	public static final String MI_SCHEMA_FE = "$" + DB_SCHEMA_FLOW_ENGINE;

	public static final String SMS_INSTANCE_TYPE_NAME = "Sms DaemonInstance Type";
	public static final String USSD_INSTANCE_TYPE_NAME = "Ussd DaemonInstance Type";
	public static final String OUTBOUND_INSTANCE_TYPE_NAME = "Outbound DaemonInstance Type";
	public static final String INBOUND_INSTANCE_TYPE_NAME = "Inbound DaemonInstance Type";
	public static final String EVENT_INSTANCE_TYPE_NAME = "Event DaemonInstance Type";
	public static final String FLOW_INSTANCE_TYPE_NAME = "Flow DaemonInstance Type";
	public static final String DEFAULT_GALDOR_PARAMETER_REFRESHER_URL = "Galdor/remoting/httpinvoker/GaldorSystemParametersRefresherService";
	public static final String DEFAULT_GALDOR_DAEMON_SCHEDULER_URL = "Galdor/remoting/httpinvoker/DaemonSchedulerRemoteService";

	public static final Long DEFAULT_INSTANCE_TYPE_ID = 0l;
	public static final Long SMS_INSTANCE_TYPE_ID = 1l;
	public static final Long USSD_INSTANCE_TYPE_ID = 2l;
	public static final Long FLOW_INSTANCE_TYPE_ID = 3l;
	public static final Long EVENT_INSTANCE_TYPE_ID = 4l;
	public static final Long OUTBOUND_INSTANCE_TYPE_ID = 5l;
	public static final Long INBOUND_INSTANCE_TYPE_ID = 6l;
	public static final String SCHEMA_HOLDER = "$SCHEMA";
}
