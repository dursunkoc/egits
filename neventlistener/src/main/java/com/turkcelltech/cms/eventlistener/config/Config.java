package com.turkcelltech.cms.eventlistener.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	// TODO_LOW handle Config
	// Completed.
	public static final String SYSTEM_DATE_FORMAT_LONG = com.aric.myel.Config.SYSTEM_DATE_FORMAT_LONG;

	public static final String WS_JSON_PARAMETER_CAMPAIGNID = "campaignId";

	public static final String WS_JSON_PARAMETER_CAMPAIGNGROUPID = "campaignGroupId";

	public static Character _MAJORDELIMITER = 30;

	public static final Integer CODE_ENABLED = 2;

	public static final Integer CODE_CAMPAIGN_STATUS_SCHEDULED = 3;

	public static final int EVENT_HANDLER_THREAD_POOL_SIZE = 10;

	public static final String ENV_NAME_EVENT_ATTRIBUTE_PREFIX = "eventAttribute";

    @Autowired
    private ApplicationContext applicationContext;
}
