/**
 * 
 */
package com.turkcelltech.cms.eventlistener.util;

import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aric.myel.statements.components.Environment;
import com.google.common.base.Objects;
import com.turkcelltech.cms.eventlistener.domain.Event;
import com.turkcelltech.cms.eventlistener.domain.EventAttributeWithValue;
import com.turkcelltech.cms.eventlistener.domain.EventHandler;
import com.turkcelltech.cms.eventlistener.domain.FlowPool;

/**
 * @author TTDKOC
 * 
 */
public final class EventHandlerCall implements Callable<FlowPool> {
	private static final Logger logger = LoggerFactory
			.getLogger(EventHandlerCall.class);
	private final EventHandler eventHandler;
	private final Event event;
	private final List<EventAttributeWithValue> attributeWithValues;
	private final Environment env;

	/**
	 * @param eventHandler
	 * @param event
	 * @param attributeWithValues
	 * @param env
	 */
	public EventHandlerCall(EventHandler eventHandler, Event event,
			List<EventAttributeWithValue> attributeWithValues, Environment env) {
		this.event = event;
		this.eventHandler = eventHandler;
		this.attributeWithValues = attributeWithValues;
		this.env = env;
	}

	/**
	 * Computes a result, or throws an exception if unable to do so.
	 * 
	 * @return computed result
	 * @throws Exception
	 *             if unable to compute a result
	 */
	@Override
	public FlowPool call() throws Exception {
		FlowPool result = null;
		if (EventUtil.handlerCriteriaSatisfied(eventHandler, event, env)) {
			String flowInitEnvironment = EventUtil.createFlowInitEnvironment(
					event, eventHandler, attributeWithValues);
			result = FlowPool.createWithParameters(event.getContextKey(),
					event.getEventSourceId(), eventHandler.getFlowId(),
					flowInitEnvironment);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("{} is handled as {}", event.toString(), Objects
					.firstNonNull(result, "NO FLOW").toString());
		}
		return result;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("event", event)
				.add("eventHandler", eventHandler).toString();
	}
}
