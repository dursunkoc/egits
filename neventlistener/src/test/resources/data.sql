--COMET_CORE
----TCODE
Insert into COMET_CORE.TCODE
   (NCODE_ID, TCODE, CLANGCODE, NVALUE, TDESC, CENABLED, NORDER, TUSER, DDATE, TMODIFYUSER, DMODIFYDATE)
 Values (1, 'CEVENTAGENT', 1, 1, 'BILLING', 2, 1, 'SYSTEM', TO_DATE('10/14/2010 15:27:26', 'MM/DD/YYYY HH24:MI:SS'), 'SYSTEM', TO_DATE('10/14/2010 15:27:26', 'MM/DD/YYYY HH24:MI:SS'));
Insert into COMET_CORE.TCODE
   (NCODE_ID, TCODE, CLANGCODE, NVALUE, TDESC, CENABLED, NORDER, TUSER, DDATE, TMODIFYUSER, DMODIFYDATE)
 Values
   (10034, 'CATTRIBUTETYPE', 1, 1, 'NUMBER_', 
    1, 1, 'SYSTEM', TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'), 'SYSTEM', 
    TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'));
Insert into COMET_CORE.TCODE
   (NCODE_ID, TCODE, CLANGCODE, NVALUE, TDESC, CENABLED, NORDER, TUSER, DDATE, TMODIFYUSER, DMODIFYDATE)
 Values
   (10035, 'CATTRIBUTETYPE', 1, 2, 'STRING', 
    2, 2, 'SYSTEM', TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'), 'SYSTEM', 
    TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'));
Insert into COMET_CORE.TCODE
   (NCODE_ID, TCODE, CLANGCODE, NVALUE, TDESC, CENABLED, NORDER, TUSER, DDATE, TMODIFYUSER, DMODIFYDATE)
 Values
   (10036, 'CATTRIBUTETYPE', 1, 3, 'DATE', 
    2, 3, 'SYSTEM', TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'), 'SYSTEM', 
    TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'));
Insert into COMET_CORE.TCODE
   (NCODE_ID, TCODE, CLANGCODE, NVALUE, TDESC, CENABLED, NORDER, TUSER, DDATE, TMODIFYUSER, DMODIFYDATE)
 Values
   (10037, 'CATTRIBUTETYPE', 1, 7, 'BOOLEAN', 
    1, 4, 'SYSTEM', TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'), 'SYSTEM', 
    TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'));
Insert into COMET_CORE.TCODE
   (NCODE_ID, TCODE, CLANGCODE, NVALUE, TDESC, CENABLED, NORDER, TUSER, DDATE, TMODIFYUSER, DMODIFYDATE)
 Values
   (10038, 'CATTRIBUTETYPE', 1, 9, 'NUMBER', 
    2, 4, 'SYSTEM', TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'), 'SYSTEM', 
    TO_DATE('10/14/2010 15:27:22', 'MM/DD/YYYY HH24:MI:SS'));
----DAEMONINSTANCE
Insert into COMET_CORE.DAEMONINSTANCE
   (ID, NAME, DAEMONINSTANCETYPE_ID, CMSAPPLICATION_INSTANCE_ID)
 Values (0, 'DEFAULT', 0, 1);
----TCAMPAIGNDEFINITION
Insert into COMET_CORE.TCAMPAIGNDEFINITION
   (NCAMPAIGNDEFINITION_ID, NCAMPAIGN_ID, NCONTEXT_ID, CCAMPAIGNTYPE, CCAMPAIGNSTATUS, DSTARTDATE, DENDDATE, TUSER, DDATE, TMODIFYUSER, DMODIFYDATE, NPROCESS_PRIORITY, CCAMPAIGNRUNTYPE, CCAMPAIGNDELAYEDFLAG)
 Values (10000, 10, 5, 4, 3, TO_DATE('19.07.2009','DD.MM.YYYY'), ADD_MONTHS(CURDATE (),10) , 'LARRY', CURDATE (), 'STATUS-UPDATER', CURDATE (), 30000, 0, 0);
--COMET_EDIB
----TEVENT_HANDLER
Insert into COMET_EDIB.TEVENT_HANDLER
   (NEVENT_HANDLER_ID, NFLOWID, NEVENT_TYPE_ID, NCAMPAIGNDEFINITION_ID, CENABLED)
 Values (1, 100, 1000, 10000, 2);
----TEVENTTYPE
Insert into COMET_EDIB.TEVENT_TYPE
   (NEVENT_TYPE_ID, NEVENT_ID, TEVENT_TYPE_NAME, CEVENTAGENT, CISACTIONDEFINED, CEVENTCRITERIA)
 Values (1000, 8787, 'Refill', 1, '1', '');
----TEVENT_ATTRIBUTE
 Insert into COMET_EDIB.TEVENT_ATTRIBUTE
   (NEVENT_ATTRIBUTE_ID, NEVENT_TYPE_ID, TATTRIBUTE_NAME, CATTRIBUTETYPE)
 Values(100001, 1000, 'amount', 9);
Insert into COMET_EDIB.TEVENT_ATTRIBUTE
   (NEVENT_ATTRIBUTE_ID, NEVENT_TYPE_ID, TATTRIBUTE_NAME, CATTRIBUTETYPE)
 Values(100002, 1000, 'transactiondate', 3);
Insert into COMET_EDIB.TEVENT_ATTRIBUTE
   (NEVENT_ATTRIBUTE_ID, NEVENT_TYPE_ID, TATTRIBUTE_NAME, CATTRIBUTETYPE)
 Values(100003, 1000, 'channel', 2);