package com.turkcelltech.cms.eventlistener.persistence;

import com.turkcelltech.cms.daemoninstance.domain.DaemonInstance;
import com.turkcelltech.cms.eventlistener.domain.FlowPool;

/**
 * Created with IntelliJ IDEA.
 * User: TTDKOC
 * Date: 10/10/13
 * Time: 10:18 PM
 * To change this template use File | Settings | File Templates.
 */
public interface FlowPoolRepository {

	/**
	 * @param daemonInstanceForEventType
	 * @param flowPools
	 */
	public void insertFlowPools(DaemonInstance daemonInstanceForEventType,
			FlowPool[] flowPools);
}
