/**
 * 
 */
package com.turkcelltech.cms.eventlistener.persistence;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.turkcelltech.cms.eventlistener.domain.EventHandler;

/**
 * @author TTDKOC
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testConfiguration.xml")
public class EventHandlerRepositoryJdbcImplTest {

	@Autowired
	private EventHandlerRepository eventHandlerRepository;

	@Test
	public void testGetActiveEventHandlersForEventTypeId() {
		Long eventTypeId = 1000l;
		List<EventHandler> result = eventHandlerRepository
				.getActiveEventHandlersForEventTypeId(eventTypeId);
		Assert.assertFalse("No Event handler found", result.isEmpty());
		for (int index = 0; index < result.size(); index++)
			Assert.assertEquals(eventTypeId, result.get(index).getEventTypeId());
	}

}
