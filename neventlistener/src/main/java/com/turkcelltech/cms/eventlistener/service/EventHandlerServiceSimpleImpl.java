/**
 *
 */
package com.turkcelltech.cms.eventlistener.service;

import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aric.myel.statements.components.Environment;
import com.google.common.util.concurrent.SettableFuture;
import com.turkcelltech.cms.eventlistener.domain.Event;
import com.turkcelltech.cms.eventlistener.domain.EventAttributeWithValue;
import com.turkcelltech.cms.eventlistener.domain.EventHandler;
import com.turkcelltech.cms.eventlistener.domain.FlowPool;
import com.turkcelltech.cms.eventlistener.util.EventUtil;

/**
 * @author TTDKOC
 */
@Service
public class EventHandlerServiceSimpleImpl implements EventHandlerService {
	private static final Logger logger = LoggerFactory
			.getLogger(EventHandlerServiceSimpleImpl.class);

	@Autowired
	private EventService eventService;

	/**
	 * @param eventHandler
	 * @param event
	 * @param attributeWithValues
	 * @param campaignId
	 * @param campaignGroupId
	 * @return
	 */
	@Override
	public Future<FlowPool> handle(EventHandler eventHandler, Event event,
			List<EventAttributeWithValue> attributeWithValues, Environment env) {
		logger.debug("{} is handled!", event.toString());

		SettableFuture<FlowPool> result = SettableFuture.<FlowPool> create();
		String flowInitEnvironment = EventUtil.createFlowInitEnvironment(event,
				eventHandler, attributeWithValues);

		final FlowPool f = FlowPool.createWithParameters(event.getContextKey(),
				event.getEventSourceId(), eventHandler.getFlowId(),
				flowInitEnvironment);

		result.set(f);
		return result;
	}
}
